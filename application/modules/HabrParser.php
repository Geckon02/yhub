<?php
/**
 * Created by PhpStorm.
 * User: Geckon01
 * Date: 21.10.2018
 * Time: 2:23
 */

namespace Modules;
use PHPHtmlParser\Dom;

class HabrParser
{
    private $dom;
    const TYPE = "Habr";

    function __construct()
    {
        $this->dom = new Dom;
    }

    function get_top($offsetFrom, $count)
    {
//        $offsetFrom = $_POST['habrOffset'];
//        $count = $_POST['postsCount'];
        $posts = [];

        $this->dom->loadFromUrl('https://habr.com/all/all');
        $rawPosts = $this->dom->find('.content-list__item article');

        if($count + $offsetFrom < count($rawPosts))
        {
            $count = $count + $offsetFrom;
        }
        elseif(count($rawPosts) > $offsetFrom) {
            $count = count($rawPosts);
        } else {
            return false;
        }

        for ($i = $offsetFrom; $i < $count; $i++){
            $rawPost = $rawPosts[$i];

            $posts[$i]['type'] = HabrParser::TYPE;
            $posts[$i]['title'] = $rawPost->find('.post__title_link')->innerHTML;

            if(strpos($rawPosts[$i]->find('.post__text')->innerHTML, 'img'))
            {
                $posts[$i]['imageLink'] = $rawPost->find('.post__text')
                    ->find('img')
                    ->getAttribute('src');
            }

            $posts[$i]['text'] = $rawPost->find('.post__text')->innerHTML;
            $posts[$i]['text'] = strip_tags($posts[$i]['text']);

            $posts[$i]['author'] = $rawPost->find('.user-info__nickname')->innerHTML;
            $posts[$i]['authorLink'] = $rawPost->find('.post__user-info')->getAttribute('href');

            $posts[$i]['time'] = $rawPost->find('.post__time')->innerHTML;

            $posts[$i]['link'] = $rawPost->find('.post__title_link')
                ->getAttribute('href');
        }
        return array_values($posts);
    }

    function get_post($link)
    {
        $post = [];

        $this->dom->loadFromUrl($link);
        $rawPost = $this->dom->find('article');

        $post['title'] = $rawPost->find('.post__title-text')->innerHTML;

        $rawText = $rawPost->find('.post__text')->innerHTML;
        $post['text'] = strip_tags($rawText, '<br>');
        $post['text'] = explode('<br />', $post['text']);
        $post['text'] = array_filter($post['text'], function($value) { return $value !== " "; });
        $post['text'] = array_values($post['text']);

        $images = $rawPost->find('.post__text img');
        for ($i = 0; $i < count($images); $i++){
            $post['images'][$i] = $images[$i]->getAttribute('src');
        }

        return $post;
    }
}