<?php
/**
 * Created by PhpStorm.
 * User: Geckon01
 * Date: 11.10.2018
 * Time: 19:28
 */

namespace Modules;
use DB\Database as Database;

class Auther
{
    private $db;
    private $id;

    function __construct()
    {
        $this->db = new Database();

        $token = $_SESSION["session_token"];
        $this->id = $this->db->exucute_with_result("SELECT `user_id` FROM `user_sessions` WHERE `token`= ?", $token)["user_id"];
    }

    function register($email, $password)
    {
        $password_hashed = password_hash($password, PASSWORD_DEFAULT);
        return $this->db->exucute_sql("INSERT INTO `users` (`user_id`, `email`, `password_hash`) VALUES (NULL, ?, ?)", array($email, $password_hashed));
    }

    function auth($email, $password)
    {
        $result = $this->db->exucute_with_result("SELECT * FROM `users` WHERE `email` = ?", $email);

        if(!password_verify($password, $result['password_hash']))
        {
            return false;
        }

        $id = $result['user_id'];
        $token = md5($id . time());
        $date = date("Y-m-d H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];

        $result = $this->db->exucute_sql("INSERT INTO `user_sessions` (`token`, `auth_date`, `ip`, `user_id`) VALUES (?, ?, ?, ?)", array($token, $date, $ip, $id));

        if($result)
        {
            $_SESSION["session_token"] = $token;
            return true;
        }

        return false;
    }

    function log_out($token = null)
    {

        if(!isset($token))
        {
            $token = $_SESSION["session_token"];
        }

        if(Auther::is_auth())
        {
            return $this->db->exucute_sql("DELETE FROM `user_sessions` WHERE `user_sessions`.`token` = ?", $token);
        }
    }

    function get_sessions()
    {
        if(Auther::is_auth())
        {
            $result = $this->db->exucute_multiple_result("SELECT * FROM `user_sessions` WHERE `user_id` = ?", $this->id);

            for ($i = 0; $i <= count($result); $i++)
            {
                if($result[$i]["token"] ==  $_SESSION["session_token"])
                {
                    $result[$i]["current"] = true;
                }
            }
            return $result;
        }
    }

    function is_auth()
    {
        $token = $_SESSION["session_token"];

        $result = $this->db->exucute_with_result("SELECT * FROM `user_sessions` WHERE `token` = ?", $token);
        return (isset($result["token"]));
    }

    public static function update_time()
    {
        $db = new Database();
        $token = $_SESSION["session_token"];
        if(isset($token))
            $db->exucute_sql("UPDATE `user_sessions` SET `auth_date` = ? WHERE `user_sessions`.`token` = ?", array(date("Y-m-d H:i:s"), $token));
    }
}