<?php

namespace DB;

class Database
{
    private $pdo;
    private $config;

    function __construct() {
        $config = parse_ini_file("config.ini", true)["database"];

        $host = $config["host"];
        $name = $config["db"];
        $charset = $config["charset"];
        $dsn = "mysql:host=$host;dbname=$name;charset=$charset";

        $opt = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {
            $this->pdo = new \PDO($dsn,  $config["user"],  $config["password"], $opt);
        } catch (\PDOException $e) {
            echo("500 Internal Server Error");
        }
    }

    function remove_html($rawString)
    {
        $rawString = strip_tags($rawString);
        $rawString = stripslashes($rawString);
        $rawString = htmlentities($rawString);

        return $rawString;
    }

    function exucute_with_result($query, $params)
    {
        $query = $this->remove_html($query);
        $params = $this->remove_html($params);

        $prepeared = $this->pdo->prepare($query);

        if(count($params) < 2)
        {
            $prepeared->bindParam(1, $params);
            $prepeared->execute();
            $result = $prepeared->fetch();
            return $result;
        }

        $prepeared->execute($params);
        $result = $prepeared->fetch();
        return $result;
    }

    function exucute_multiple_result($query, $params)
    {
        $query = $this->remove_html($query);

        $prepeared = $this->pdo->prepare($query);

        if(count($params) < 2)
        {
            $prepeared->bindParam(1, $params);
            $prepeared->execute();
            $result = $prepeared->fetchAll();
            return $result;
        }

        $prepeared->execute($params);
        $result = $prepeared->fetchAll();
        return $result;
    }

    function exucute_sql($query, $params)
    {
        $query = $this->remove_html($query);

        $prepeared = $this->pdo->prepare($query);

        if(count($params) < 2)
        {
            $prepeared->bindParam(1, $params);
            return $prepeared->execute();
        }
        return $prepeared->execute($params);
    }
}