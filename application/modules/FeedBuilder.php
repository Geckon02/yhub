<?php
/**
 * Created by PhpStorm.
 * User: Geckon01
 * Date: 24.10.2018
 * Time: 1:12
 */

namespace Modules;


class FeedBuilder
{
    private $pikabu;
    private $habr;
    private $resultFeed;

    function __construct()
    {
        $this->habr = new \Modules\HabrParser();
        $this->pikabu = new \Modules\PikabuParser();
        $this->resultFeed = array();
    }

    public function AddHabr($offset, $count)
    {
        $posts = $this->habr->get_top($offset, $count);

        foreach ($posts as $post)
        {
            array_push($this->resultFeed, $post);
        }
    }

    public function Build()
    {
        return $this->resultFeed;
    }
}