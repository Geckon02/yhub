<?php

class Model_Api extends Model
{
    private $habr;
    private $feedBuilder;
    private $auther;

    function __construct()
    {
        $this->auther = new \Modules\Auther();
        $this->habr = new \Modules\HabrParser();
        $this->feedBuilder = new \Modules\FeedBuilder();
    }
    public function signup($email, $password)
    {
        $result = $this->auther->register($email, $password);

        $json = json_encode(array(
            "status" => $result
        ));
        return $json;
    }

    public function auth($email, $password)
    {
        if($this->auther->auth($email, $password))
        {
            $json = json_encode(array(
                "status" => true,
                "token" => $_SESSION["session_token"]
            ));

            return $json;
        }

        $json = json_encode(array(
            "status" => false,
            "error" => "Auth error. Password or email incorrect"
        ));

        return $json;
    }

    function logout($token = null)
    {
        $result = $this->auther->log_out($token);
        return $json = json_encode(array(
            "status" => $result,
        ));
    }

    function sessions()
    {
        if(!$this->auther->is_auth())
            return $json = json_encode(array(
                "status" => false,
                "error" => "Unathorized"
            ));

        $result = $this->auther->get_sessions();

        return $json = json_encode(array(
            "status" => true,
            "sessions" => $result
        ));
    }

    function get_feed()
    {
        if(!$this->auther->is_auth())
            return $json = json_encode(array(
                "status" => false,
                "error" => "Unathorized"
            ));

        $this->feedBuilder->AddHabr($_POST['offset'], $_POST['postsCount']);
        $result = $this->feedBuilder->Build();

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    function get_post($type, $link)
    {
        $result = $this->habr->get_post($link);
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}
