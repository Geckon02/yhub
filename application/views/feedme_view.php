<body class="full-screen-container" xmlns:v-on="http://www.w3.org/1999/xhtml">
<section id="container" class="container">
    <div id="sessions-preloader" class="p-2 col-centered" v-if="preloader">
        <div class="cssload-container">
            <div class="cssload-whirlpool"></div>
        </div>
    </div>
    <article class="card m-2" v-for="post in posts">
        <div class="card-header">
            <p id="source-type"><b>{{post.type}}</b></p>
            <i class="fas fa-user"> <a target="_blank" class="m-1" :href="post.authorLink">{{post.author}} </a>
            </i><i class="fas fa-calendar-alt"> {{post.time}}</i>
        </div>
        <div class="card-body">
            <h5 class="card-title text-center">{{post.title}}</h5>
            <p class="card-text">{{post.text}}</p>
            <img class="card-img offset-lg-1 offset-0" :src="post.imageLink" style="max-width: 55rem;">
        </div>
        <div class="card-footer">
            <a target="_blank" :href="post.link" class="btn btn-block">Читать в источнике</a>
            <a target="_blank" :href="/read/ + post.type + '/?link=' + post.link" class="btn btn-block">Читать здесь</a>
        </div>
    </article>
    <button class="btn btn-info btn-lg col-12 m-2" v-if="!preloader && !endOfFeed && !loading" v-on:click="loadMore" data-loading-text="<i class='fa fa-spinner fa-spin '></i>Ищем">Хочу еще!</button>
    <button id="more-btn" class="btn btn-info btn-lg col-12 m-2 disabled" v-if="!preloader && !endOfFeed && loading"><i class='fa fa-spinner fa-spin '></i> Ищем</button>
    <button class="btn btn-danger disabled col-12 m-2" v-if="endOfFeed">Это конец :-(</button>
</section>
    <script src="../../js/src/feed.js"></script>
</body>