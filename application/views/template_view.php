<!DOCTYPE html>
<?php $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
$auther = new \Modules\Auther();
$isauth = $auther->is_auth();
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=0.7">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Feedpoll</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo $host?>css/style.css">
    <link rel="stylesheet" href="<?php echo $host?>css/preloader.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/solid.css" integrity="sha384-osqezT+30O6N/vsMqwW8Ch6wKlMofqueuia2H7fePy42uC05rm1G+BUPSd2iBSJL" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/regular.css" integrity="sha384-4e3mPOi7K1/4SAx8aMeZqaZ1Pm4l73ZnRRquHFWzPh2Pa4PMAgZm8/WNh6ydcygU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/fontawesome.css" integrity="sha384-BzCy2fixOYd0HObpx3GMefNqdbA7Qjcc91RgYeDjrHTIEXqiF00jKvgQG0+zY/7I" crossorigin="anonymous">
    <link rel="icon" href="<?php echo $host?>/images/logo.png">
    <script src="<?php echo $host?>js/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    <script src="<?php echo $host?>js/bootstrap.min.js"></script>
</head>
<?php if($isauth): ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo ($isauth) ? '/feedme': '/';?>">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/settings">Настройки</a>
                </li>
                <li class="nav-item disabled">
                    <a class="nav-link disabled" href="#">Скоро</a>
                </li>
                <li class="nav-item disabled">
                    <a class="nav-link disabled" href="#">Скоро</a>
                </li>
            </ul>
        </div>
    </nav>
<?php endif; ?>
<?php include 'application/views/'.$content_view; ?>
</html>