<?php
$auther = new \Modules\Auther();
?>
<body class="full-screen-container" xmlns:v-on="http://www.w3.org/1999/xhtml">
<?php if (!$auther->is_auth()): ?>
    <section class="col-md-12">
        <div id="container" class="container">
            <div class="panel panel-box col-md-5 col-sm-4 col-ld-6 p-4 col-centered">
                <div class="panel-heading p-2">
                        <div class="row">
                            <h3 class="text-center text-italic"><a href="#" id="SignInToggle" v-on:click="showSignIn">SignIn</a><b> or </b><a href="#" id="SignUpToggle" v-on:click="showSignUp">SignUp</a></h3>
                        </div>
                    </div>
                <div class="panel-body p-2">
                        <div class="p-3" id="signInForm" v-if="isSignIn">
                            <form id="signIn" action="/api/auth" method="post" v-on:submit="submitForm">
                                <div class="form-group">
                                    <label for="email"><b>Email</b></label>
                                    <input id="email" type="email" class="form-control" name="username" placeholder="Email" required >
                                </div>
                                <div class="form-group">
                                    <label for="password"><b>Пароль</b></label>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Пароль" required>
                                </div>
                                <input type="submit" class="col-md-12 col-sm-12 col-lg-12 btn btn-primary" value="Sign In">
                            </form>
                        </div>
                        <div class="p-3" id="signUpForm" v-if="!isSignIn">
                            <form id="signUp" action="/api/signup" method="post" v-on:submit="submitForm">
                                <div class="form-group">
                                    <label for="email"><b>Email</b></label>
                                    <input id="email" type="email" class="form-control" name="username" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <label for="password"><b>Пароль</b></label>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Пароль" required minlength="6">
                                </div>
                                <input type="submit" class="col-md-12 col-sm-12 col-lg-12 btn btn-primary" value="Sign Up">
                            </form>
                        </div>
                </div>
                <div id="sessions-preloader" class="p-2" v-if="preloader">
                    <div class="cssload-container">
                        <div class="cssload-whirlpool"></div>
                    </div>
                </div>
                <div class="alert alert-danger" style="display: none;"></div>
            </div>
        </div>
    </section>
    <script src="../../js/src/main.js"></script>
<?php else: ?>
    <div class="container">
        <section class="card col-md-8 col-lg-10 col-sm-6 col-centered">
            <div class="card-body">
                <h5 class="card-title">Select your feed sources</h5>
                <form class="ml-5" action="" method="post">
                    <label class="material-checkbox">
                        <input type="checkbox">
                        <span>Source1</span>
                    </label>
                    <input type="submit" class="btn btn-primary col-md-12 mt-3" value="Proceed">
                </form>
            </div>
        </section>
    </div>
<?php endif; ?>
</body>
