<body class="full-screen-container" xmlns:v-on="http://www.w3.org/1999/xhtml">
<script src="js/vue/table.vue"></script>
<div id="container" class="container">
    <div class="card">
        <h5 class="card-header">Активные подключения</h5>
        <div class="card-body">
            <div id="sessions-preloader" v-if="preloader">
                <div class="cssload-container col-centered">
                    <div class="cssload-whirlpool"></div>
                </div>
            </div>
            <table class="table" v-if="sessions.length">
                <thead>
                <tr>
                    <th scope="col">Последний вход</th>
                    <th scope="col" v-if="window.w > 720">Ip</th>
                    <th scope="col" v-if="window.w > 720">Статус</th>
                    <th scope="col">Дейсвтия</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="session in sessions" :class="{ 'table-primary' : session.current == true}">
                    <td>
                        {{session.date}}
                    </td>
                    <td v-if="window.w > 720">
                        {{session.ip}}
                    </td>
                    <td v-if="window.w > 720">
                        <p v-if="session.current == true">Текущее подключение</p>
                    </td>
                    <td>
                        <div class="row" v-on:click="removeSession">
                            <i title='End session' class="fas fa-trash"></i>
                            <input type='hidden' :value="session.token">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
<script src="../../js/src/settings.js"></script>
<script src="js/jquery-dateformat.min.js"></script>
</body>
