<body class="full-screen-container" xmlns:v-on="http://www.w3.org/1999/xhtml">
<section class="card m-2 col-md-11 mr-auto ml-auto" v-if="!preloader">
    <div class="card-header">
        <h1 class="text-center">{{post.title}}</h1>
    </div>
    <div class="card-body col-md-10 ml-auto mr-auto">
        <blockquote class="blockquote">
            <p v-for="paragraph in post.text">{{paragraph}}<br><br></p>
        </blockquote>
        <div class="container text-center">
            <img class="col-4 m-1 border rounded" v-on:click="openImagePriview($event)" :src="image" alt=""  v-for="image in post.images">
        </div>
    </div>
</section >
<input id="postlink" type="hidden" data-postlink="<?= $data['link']?>">
<div class="modal fade" id="imagemodal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="" id="imagepreview" class="mh-80 mw-90">
            </div>
            <button type="button" class="btn btn-block" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
<script src="../../js/src/read.js"></script>
</body>