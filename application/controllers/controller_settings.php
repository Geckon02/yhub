<?php

class Controller_Settings extends Controller
{
    private $auther;

    function __construct()
    {
        $this->view = new View();
        $this->auther = new \Modules\Auther();
    }

    function action_index()
    {
        if(!$this->auther->is_auth())
        {
            header("HTTP/1.1 401 Unauthorized");
            $this->view->generate('error_view.php', 'template_view.php', "401 Unauthorized");
            exit;
        }

        $this->view->generate('settings_view.php', 'template_view.php');
    }

}