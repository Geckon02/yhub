<?php

class Controller_Api extends Controller
{
    private $habr;

    function __construct()
    {
        $this->model = new Model_Api();
    }

    function action_index()
    {
        echo json_encode(array(
            "status" => false,
            "error" => "No action"
        ));
    }

    function  action_signup()
    {
        echo $this->model->signup($_POST["username"], $_POST["password"]);
    }

    function action_auth()
    {
        echo $this->model->auth($_POST["username"], $_POST["password"]);
    }

    function action_logout()
    {
        echo $this->model->logout($_POST["token"]);
    }

    function action_sessions()
    {
        echo $this->model->sessions();
    }

    function action_feed()
    {
        echo $this->model->get_feed();
    }

    function action_habr_post()
    {
        echo $this->model->get_post('Habr', $_GET['link']);
    }
}