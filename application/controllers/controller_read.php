<?php

class Controller_Read extends Controller
{
    private $auther;
    private $api;

    function __construct()
    {
        $this->view = new View();
        $this->auther = new \Modules\Auther();
    }

    function action_habr()
    {
        if(!$this->auther->is_auth())
        {
            header("HTTP/1.1 401 Unauthorized");
            $this->view->generate('error_view.php', 'template_view.php', "401 Unauthorized");
            exit;
        }

        $data = $_GET;
        $this->view->generate('read_view.php', 'template_view.php', $data);
    }

}