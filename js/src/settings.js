;(function () {
    var app = new Vue({
        el: '#container',
        data: {
            sessions: [],
            preloader: true,
            window: {
                w: 0,
                h: 0
            }
        },
        created: function() {
            window.addEventListener('resize', this.handleResize)
            this.handleResize();
        },
        destroyed: function() {
            window.removeEventListener('resize', this.handleResize)
        },
        mounted: function () {
            this.$nextTick(function () {
                $.post("/api/sessions", function (result) {

                    $.parseJSON(result).sessions.forEach(function (session) {
                        app.sessions.push(
                            {
                                date: $.format.prettyDate(session['auth_date']),
                                ip: session['ip'],
                                token: session['token'] ,
                                current: session["current"]
                            });
                    });

                    app.preloader = false;
                });
            })
        },
        methods: {
            handleResize() {
                this.window.w = window.innerWidth;
                this.window.h = window.innerHeight;
            },
            removeSession (event){
                let session_token = $(event.target).parent().find('input').val();

                $.post("/api/logout", { token: session_token },
                    function(result){
                        $(event.target).closest('tr').hide("medium");
                    }).fail(function(result){
                    console.log(result);
                });
            }
        }
    });
}());