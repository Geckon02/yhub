;(function () {
    var app = new Vue({
        el: '.card',
        data: {
            post: [],
            preloader: true
        },
        mounted: function () {
            $.get("/api/habr_post", {'link': $('#postlink').data('postlink')}, function (result) {
                app.post = $.parseJSON(result);
                app.preloader = false;
            })
        },
        methods:{
            openImagePriview(e)
            {
                $('#imagepreview').attr('src', $(e.target).attr('src'));
                $('#imagemodal').modal('show');
            }
        }
    });
}());
