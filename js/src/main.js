;(function () {
    var app = new Vue({
        el: '#container',
        data: {
            isSignIn: true,
            preloader: false
        },
        methods: {
            showSignUp()
            {
                app.isSignIn = false;
                console.log(app.isSignIn);
            },
            showSignIn()
            {
                app.isSignIn = true;
                console.log(app.isSignIn);
            },
            showError(message)
            {
                $('.alert').text(message).show("medium");
                setTimeout(function () {$('.alert').hide('fast')}, 2000);
            },
            submitForm(e)
            {
                e.preventDefault();

                let form = $("form");

                $('input[type=submit]').hide('fast');
                app.preloader = true;

                $.ajax({
                    url: form.attr('action'),
                    type: "POST",
                    data: form.serialize(),
                    success: function (result) {
                        let json = $.parseJSON(result);
                        console.log(json);

                        if(!json.status)
                            app.showError(json.error);
                        else location.reload();

                        $('input[type=submit]').show('fast');
                        app.preloader = false;
                    },
                    error: function (result) {
                        console.log(result);
                        app.showError(result.responseText + result.statusText);

                        $('input[type=submit]').show('fast');
                        app.preloader = false;                        $('input[type=submit]').show('fast');
                        app.preloader = false;
                    }
                })
            }
        }
    });
}());
