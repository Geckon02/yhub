;(function () {
    var app = new Vue({
        el: '#container',
        data: {
            posts: [],
            preloader: true,
            endOfFeed: false,
            loading: false
        },
        mounted: function () {
            $.post("/api/feed", {'offset': 0, 'postsCount': 3}, function (result) {
                console.log( $.parseJSON(result));
                app.posts = $.parseJSON(result);
                app.preloader = false;
            })
        },
        methods:{
            loadMore(e){
                app.loading = true;
                let habrOffset = 0;

                $('article').find('#source-type').each(function () {
                    if($(this).text() === 'Habr')
                        habrOffset++;
                });

                $.post("/api/feed", {'offset': habrOffset, 'postsCount': 3}, function (result) {
                    result = $.parseJSON(result);

                    if(!result){
                        app.endOfFeed = true;
                        return;
                    }

                    for (let i = 0; i < result.length; i++){
                        app.posts.push(result[i]);
                    }
                    app.loading = false;
                }).fail(function () {
                    alert("Ошибка загрузки!");
                    app.loading = false;
                });
            }
        }
    });
}());
